# MongoDB Atlas

Atlas is a service provided by MongoDB themselves which basically empowers you with a free 512MB-sized MongoDB cloud server to which you can connect. It's really good for starting bots! 😊

To start just go over to [**https://www.mongodb.com/cloud/atlas**] and click on **`Start Free`**.

After finishing up the registration, you'll be prompted with the `Create New Cluster` tab. It should look something like this:

![Create New Cluster](./imgs/create-cluster.png)

You now need to choose one of the providers and the closest region to you.
After that scroll to the bottom and edit the cluster name (defaults to `Cluster0`), if you want of course. Finally, click `Create Cluster` and complete the CAPTCHA.
Now, you should have been redirected to your clusters dashboard.

![Cluster Dashboard](./imgs/cluster-dashboard.png)

You can now click the little `Get Started` button and complete the setup!

![Complete Setup](./imgs/complete-setup.png)

Note: If you want a nice looking and powerful MongoDB client to test and fidget around with your new shiny cluster, I suggest you take a look at [**MongoDB Compass**](https://www.mongodb.com/products/compass)

---

We're finished here! Let's go over to [Start Python Coding](./start-py-coding.html)!
