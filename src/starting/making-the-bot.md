# Making the Bot App 
***by `God Empress Verin#6160`***

---

In this mini-guide we'll be covering how to create a bot app and how to add it to a server. Let's go!

To begin, follow [this](https://discordapp.com/developers/applications/) URL and login to your Discord account.  
Once there, ensure you are on the applications tab and click on "Create an application"

![Create an Application](./imgs/create-an-app.png)

Your should now be looking at a page like this

![Application Overview](./imgs/app-overview.png)

From here, you can give your application a name and provide an image to be used for your application  Before proceeding, make a note of your client ID! 
You will need it later when we go to add your bot to a guild! For now though, we are going to go to this tab on the left and click on "Bot"

![Click on "Bot"](./imgs/click-bot-tab.png)

Next, click on "Add Bot"

![Click on "Add Bot"](./imgs/click-add-bot.png)

You will receive a prompt that looks like this, simply click on "Yes, do it!"

![Click "Yes"](./imgs/click-bot-yes.png)

You've successfully created a bot account! From here, you can give your bot a name and avatar. Unfortunately we have no way for our bot to communicate with discord yet. To do this, we'll generate a bot token!   
**An important disclaimer here, do NOT reveal your bot token to anyone ever! Anybody with your bot token has the ability to commit actions on behalf of your bot! This includes, but is not limited to, channel deleting, kicking users from guilds, spamming users DMs, and many more harmful actions. If your token has already been leaked, please follow the steps in [Token Leaked](../faqs/token-leaked.html) to remedy the situation!**

To get your bots token, simply click on "Copy"

![Click on "Copy"](./imgs/click-copy-token.png)

Simply paste the bot token into the appropriate location on whatever bot you are trying to use. Self-hostable bots like Nadeko and Fredboat typically have a credentials file where this would go, however if you are creating your own bot, this will depend on the language you are using as well as the api wrapper you've chosen and how you have decided to store such values. **Please, please, please, if you have your bot on any VCS and you have your API key hardcoded in, please ensure that the key is removed before pushing changes! If you have accidentally posted your token onto your VCS, please refer to the previous disclaimer and [Token Leaked](../faqs/token-leaked.html)!**

Lastly, we'll invite our newly created bot to a server. If you don't want other people to be able to add your bot to servers, please ensure the "Public Bot" switch is toggled off on your bots application page so it looks like this

![Public Bot Toggle](./imgs/public-bot-toggle.png)

At the bottom of your application page, there is a permission integer generator that will provide the permission integer to be placed at the end of your bots invite link. By default, the permissions is set to 0, this means the bot will have whatever permissions the everyone role has when it joins the guild.

![Permission Generator](./imgs/perm-gen.png)

Remember that client ID from earlier? We'll need that now, replace the `INSERT_CLIENT_ID_HERE` in this URL with whatever your client ID is! If you've chosen to request specific permissions for your bot, you'll also want to replace the 0 at the end with whatever the permissions integer is from the generator!

> https://discordapp.com/oauth2/authorize?client_id=INSERT_CLIENT_ID_HERE&scope=bot&permissions=0

You'll now be looking at a page that looks like this

![Bot Invite Prompt](./imgs/bot-invite-prompt.png)

Remember that you need the "Manage Server" permission to add bots to a server! Any guilds you don't have that permission in simply won't show up in the dropdown!

Thats it! With any luck, your new bot account should now be showing up in the members list of whatever guild you've added it to! Or if you've run into a snag and need help, our helpers and staff team over on the Discord server are always willing to provide assistance! Just shoot a question in the `#other-help` channel and one of us will get to you as soon as possible!



