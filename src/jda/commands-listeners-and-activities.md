# Commands, Listeners and Activities

Now we're going to make the commands for our bot!

I'm just going to make a simple ping-pong command, you can see the docs here if you want insight on what you'd want to do, I highly recommend you do read them [here](https://ci.dv8tion.net/job/JDA/javadoc/).

For making other commands, you can go to the docs like it says above! and ask us in the [**`jvm-help`**](https://discordapp.com/channels/265828729970753537/651854967778443265) channel on our Discord server if you've got any questions!

Now, we're gonna make a new class that extends ListenerAdapter!

For that, I'm going to add a class to the `src` folder (or create a new folder and call it commands and add it in there, you also can create folders and name them based on the catgeory of the command you want to add, e.g. tictactoe belongs in fun) and name the class `PingCmd` (the `Listeneradapters` behave as events and are used to make "commands", you can use them to make your own command framework).

```java
public class PingCmd extends ListenerAdapter {
    // code
}
```

Also, this is going to be much simpler to understand if you have some experience in Java!

Now, in the class, we're going to override the `onMessageReceived` method! This is an event that triggers when a message is sent in a guild that the bot is in! We use it like this:

> Please read the comments otherwise its going to be hard to understand!

```java
@Override
public void onGuildMessageReceived(MessageReceivedEvent event) {
    // This statement checks if the author of the message 
    // sent is a bot, and if the author of the message 
    // is a bot, it doesnt respond.
    if(event.getAuthor().isBot()) {
        return;
    }

    // This if statement checks if the message is in DMs and
    // ignores it if it is, if you dont want that to happen,
    // delete or modify this line to your needs
    if(event.isFromType(ChannelType.PRIVATE)) {
        return;
    }

    // Gets the message that triggered the event.
    Message msg = event.getMessage();

    // `msg.getContentRaw()` gets the message's content with the 
    // markdown and hidden characters, even the \ if it happened
    // to be after a symbol, in which Discord makes it disappear.
    if(msg.getContentRaw().equals("!ping")) {
        MessageChannel channel = event.getChannel();
        channel.sendMessage("Pong!").queue();
        // Important to call `.queue()`.
    }

}
```

Here, you could realize that reading the comments would be very useful! 

This code would not work with the bot if we run it, thats because we haven't added it as a listener to the bot! If you remember the `JDABuilder#createDefault` we used to make our bot come online, we're going to go back to it.

```java
JDA jda = JDABuilder.createDefault(token);

jda.awaitReady();
```

In this code, we add a simple line to register our listener! the line is `.addEventListener(new PingCmd())` and add it right before the `.build()` line!

> **NOTICE:** You can replace `PingCmd` in the `.addEventListener(new PingCmd())` line to your class's (that extends `ListenerAdapter`) name followed by ().

Now, the `JDABuilder` code is:

```java
JDA jda = JDABuilder.createDefault(token)
    .addEventListener(new PingCmd())
    .build();

jda.awaitReady()
```

You can add as many listeners as you want!

Which would lead us to the full code:

```java
public class Main {
    public static void main(String[] args) throws LoginException {
        JDA jda = JDABuilder.createDefault(token)
            .addEventListener(new PingCmd())
            .build();

        jda.awaitReady();
    }
}

// in another class called PingCmd
public class PingCmd extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        // This statement checks if the author of the message 
        // sent is a bot, and if the author of the message is
        // a bot, it doesnt respond.
        if(event.getAuthor().isBot()) {
            return;
        }

        // This if statement checks if the message is in DMs and
        // ignores it if it is, if you dont want that to happen,
        // delete or modify this line to your needs
        if(event.isFromType(ChannelType.PRIVATE)) {
            return;
        }

        // Gets the message that triggered the event
        Message message = event.getMessage();

        if(msg.getContentRaw().equals("!ping"))
            // msg.getContentRaw() gets the message's content 
            // with the markdown and hidden characters, even the
            // \ if it happened to be after a symbol, in which 
            // Discord makes it disappear.
        {
            MessageChannel channel = event.getChannel();
            channel.sendMessage("Pong!").queue();
            // Important to call .queue()
        }
    }
}
```

So... You know the cool bots, that have custom amazing playing status?
I'm going to teach you all about it today!

The playing status thing doesnt need a genius to make it, and its quite simple! But, we need to get to our `JDABuilder` lines again! Those are pretty important if you couldn't already tell.

```java
JDA jda = JDABuilder.createDefault(token)
    .addEventListener(new PingCmd())
    .build();

jda.awaitReady();
```

We need to add another line above the `.build()` line again!
See? This is getting simple and easy!

Here's the line to make a playing status:
`.setActivity(Activity.playing("gameNameHere"))`
This will show: `Playing  gameNameHere`

You can change the `gameNameHere` to make it show anything, like this:
`.setActivity(Activity.playing("With Finn & Jake"))`
This will display: `Playing with Finn & Jake` as your bot's status! 

This is what the JDABuilder lines would look like:

```java
JDA jda = JDABuilder.createDefault(token)
    .addEventListener(new PingCmd())
    .setActivity(Activity.playing("with Finn & Jake"))
    .build();

jda.awaitReady();
```

Now, on to watching and listening to and streaming statuses!

You could switch `Activity.playing` with any status you want!
For example: `Activity.listening("you")` which will show: `Listening to you`

> **Note:** You can also use the `setStatus` function to set the status of your bot!

E.g:
```java
JDA jda = JDABuilder.createDefault(token)
    .addEventListener(new PingCmd())
    .setActivity(Activity.playing("with Finn & Jake"))
    .setStatus(OnlineStatus.DO_NOT_DISTURB)
    .build();

jda.awaitReady();
```

And since I never explained what `jda.awaitReady();` does, it is blocking the bot to guarantee that JDA will be completely loaded before working. 
This means that every line after it will only run after JDA loads.

E.g:
If I add `System.out.println("JDA successfully loaded!");` after that line, it will only print that after JDA loads, because `jda.awaitReady();` is blocking.


> **NOTE:** This is not only limited to prints, but to any other line after `jda.awaitReady()`.

---

Whenever you're ready, go into [**Embeds**](./embeds.html).
